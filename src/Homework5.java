import java.util.Scanner;

public class Homework5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int min = a;
        int b;
        while (a != -1) {
            while (a != 0){
                b = a % 10;
                a = a / 10;
                if (min > b) {
                    min = b;
                }
            }
            a = scanner.nextInt();
        }
        System.out.println(min);
    }
}
